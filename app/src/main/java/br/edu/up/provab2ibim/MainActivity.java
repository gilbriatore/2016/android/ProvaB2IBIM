package br.edu.up.provab2ibim;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  public void apostar(View v) {
    EditText cx1 = (EditText) findViewById(R.id.txt1);
    EditText cx2 = (EditText) findViewById(R.id.txt2);
    EditText cx3 = (EditText) findViewById(R.id.txt3);
    EditText cx4 = (EditText) findViewById(R.id.txt4);
    EditText cx5 = (EditText) findViewById(R.id.txt5);

    String aposta1 = cx1.getText().toString();
    String aposta2 = cx2.getText().toString();
    String aposta3 = cx3.getText().toString();
    String aposta4 = cx4.getText().toString();
    String aposta5 = cx5.getText().toString();

    Intent intent = new Intent(this, MainActivity.class);
    intent.putExtra("aposta1", aposta1);
    intent.putExtra("aposta2", aposta2);
    intent.putExtra("aposta3", aposta3);
    intent.putExtra("aposta4", aposta4);
    intent.putExtra("aposta5", aposta5);
    startActivity(intent);
  }
}